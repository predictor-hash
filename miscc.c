#include "miscc.h"

/* register a function to be called when an error is detected */
//void
//fatal_hook(void (*fn)(FILE *stream))	/* fatal hook function */
//{
//  hook_fn = fn;
//}


/* declare a fatal run-time error, calls fatal hook function */
#ifdef __GNUC__
void
_fatal(char *file, char *func, int line, char *fmt, ...)
#else /* !__GNUC__ */
void
fatal(char *fmt, ...)
#endif /* __GNUC__ */
{
  va_list v;
  va_start(v, fmt);

  fprintf(stderr, "fatal: ");
  //myvfprintf(stderr, fmt, v);

//#ifdef __GNUC__
//  if (verbose)
//    fprintf(stderr, " [%s:%s, line %d]", func, file, line);
//#endif /* __GNUC__ */
//  fprintf(stderr, "\n");
//  if (hook_fn)
//    (*hook_fn)(stderr);
  exit(1);
}

/* declare a panic situation, dumps core */
#ifdef __GNUC__
void
_panic(char *file, char *func, int line, char *fmt, ...)
#else /* !__GNUC__ */
void
panic(char *fmt, ...)
#endif /* __GNUC__ */
{
  va_list v;
  va_start(v, fmt);

  fprintf(stderr, "panic: ");
  //myvfprintf(stderr, fmt, v);
//#ifdef __GNUC__
//  fprintf(stderr, " [%s:%s, line %d]", func, file, line);
//#endif /* __GNUC__ */
//  fprintf(stderr, "\n");
//  if (hook_fn)
//    (*hook_fn)(stderr);
//  abort();
}


/* copy a string to a new storage allocation (NOTE: many machines are missing
   this trivial function, so I funcdup() it here...) */
char *				/* duplicated string */
mystrdup(char *s)		/* string to duplicate to heap storage */
{
  char *buf;

  if (!(buf = (char *)malloc(strlen(s)+1)))
    return NULL;
  strcpy(buf, s);
  return buf;
}

