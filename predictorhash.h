#ifndef PREDICTORHASH_H
#define PREDICTORHASH_H



#define PREDICT_TABLE_SIZE 0x1000 // 256 entry table
#define INDEX_MASK 0x00000FFF // 8 bit index in a 256 entry table
#define TAG_MASK   0x00003000 // next 6 bits for tag
#define TAG_SHIFT  12	      // shift to bring the tag down for comparison.
#define HASH_SHIFT 5 // shift the prev_hash by this many bits before ORing with last_hash

struct predict_table_entry{
/*Phase prediction paper says that their predictor has 
256 entries encompassing less than 500 bytes. This should make each entry of
2 bytes (a little less).  How to divide this between tag and predicted id ?

On-line simpoint says that they have a signature table of 1024 entries. Thus
the phase_id can go up to 10 bits. This leaves only 6 bits for Tag ?  Or did
they use larger table entries ?
*/

	// limit the size in the light of above discussion?
	unsigned int tag;	
	unsigned int phase_id;
};

struct PredictorHash {

// how much in future we have to predict
	unsigned int future_len;	

	unsigned short * history;
	unsigned int hist_head;
	unsigned int hist_tail;

	/*it shows only the num-entries. size of history = hist_len*2*2 bytes
	 * if short */
	unsigned int history_length; 
	int largest_center;


	// prediction table
	// see the size : shall we limit the size ??
	struct predict_table_entry predict_table[PREDICT_TABLE_SIZE]; 
	unsigned int prev_hash;       // contains the id and num_occr of 
				      // prev (before the last) phase hashed

	unsigned int last_phase_predictions; //stats
	unsigned int new_phase_predictions;
	
};

struct PredictorHash * predictor_create( 
		  unsigned int hist_size
		  ,unsigned int futur_len
		);
#endif //PREDICTORHASH_H
