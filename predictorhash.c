#include "predictorhash.h"
#include "jenkins_hash.h"
#include <stdio.h>
#include <stdlib.h>
#define USE_JENKINS_HASH

struct PredictorHash * predictor_create( 
		  unsigned int hist_size
		  ,unsigned int futur_len
		){

	struct PredictorHash *pred = (struct PredictorHash *) 
		malloc( sizeof(struct PredictorHash) );

	pred->future_len = futur_len;
	pred->history_length = hist_size;

	//initialize the hitory
	pred->history = (unsigned short *) 
		malloc( pred->history_length *2* sizeof(unsigned short) );



	int i=0;
	for(i=0; i< pred->history_length*2; i+=2 ){
		pred->history[i] = 0; //phase id
		pred->history[i+1] = 0; //phase_cntr
	}
	pred->hist_head = 0; // to be advanced by 2
	pred->hist_tail = 0;

	//initialize the hash etc
	//predict_table = (predict_table_entry *) 
	//malloc( PREDICT_TABLE_SIZE*sizeof(predict_table_entry) );
	for( i=0; i< PREDICT_TABLE_SIZE; i++){
		pred->predict_table[i].tag = 0;
		pred->predict_table[i].phase_id = 0;
	}

	//stats
	pred->new_phase_predictions = 0;
	pred->last_phase_predictions = 0;

	return pred;     //check for NULL Ptr above , memory allocation
}

/*
 *
 */
void predictor_printhistory( struct PredictorHash * pred){

	printf(" ********* PREDICTOR *********** \n");
	if( pred == NULL ){
		printf(" predictor_printhistory: bad ptr error \n");
	}

	//initialize the hitory
	printf(" predictor hitory (id, num) = ");
	int i=0;
	int tail = pred->hist_tail;
	for(i=0; i< pred->history_length*2; i+=2 ){
		printf("(%d, %d), ", pred->history[tail] 
				    ,pred->history[tail+1] //phase_cntr
		      );
		tail = (tail+2)%(pred->history_length*2);
	}
	printf("\n");
	
	// to be advanced by 2
	printf(" history head = %d \n",pred->hist_head); 
	printf(" history tail = %d \n",pred->hist_tail); 

	printf(" ********* PREDICTOR  END *********** \n");

}

/*
 *
 */
void predictor_print( struct PredictorHash * pred){

	printf(" ********* PREDICTOR *********** \n");
	if( pred != NULL ){
		printf(" predictor: history_length = %d \n", 
				pred->history_length);
		printf(" predictor: future length  = %d \n", 
				pred->future_len);

	//initialize the hitory
	printf(" predictor hitory (id, num) = ");
	int i=0;
	for(i=0; i< pred->history_length*2; i+=2 ){
		printf("(%d, %d), ", pred->history[i] 
				    ,pred->history[i+1] //phase_cntr
		      );
	}
	printf("\n");
	// to be advanced by 2
	printf(" history head = %d \n",pred->hist_head); 
	printf(" history tail = %d \n",pred->hist_tail); 

	//initialize the hash etc
	//predict_table = (predict_table_entry *) 
	//malloc( PREDICT_TABLE_SIZE*sizeof(predict_table_entry) );
	printf(" predictor table (tag , id) = ");
	for( i=0; i< PREDICT_TABLE_SIZE; i++){
		printf("(%d, %d), ", pred->predict_table[i].tag
				    , pred->predict_table[i].phase_id
		      );
	}
	printf("\n");

	// to be advanced by 2
	printf(" num new predictions = %d \n",pred->new_phase_predictions); 
	printf(" num last phase predictions = %d \n",
			pred->last_phase_predictions); 
	}
	else
		printf(" predictor is NULL \n");
	printf(" ********* PREDICTOR  END *********** \n");

}


/*
 *
 */
void predictor_destroy( struct PredictorHash * pred){

	if( pred ){
		if( pred->history )
			free( pred->history );
		free( pred );
	}
}


/*
 *
 */
void predictor_predictfutureintervals( struct PredictorHash * pred
				,unsigned int * future_seq
				){
	//unsigned int * future_seq = (unsigned int *) 
	//malloc ( pred->future_len * sizeof(unsigned int) );

	// copy the history for local use --- [phase-id][phase-cntr]
	// each entry contains a phase_id and phase_cntr
	unsigned int hist_size = pred->history_length * 2; 
	unsigned short *hist = (unsigned short *) 
		malloc( pred->history_length *2* sizeof(unsigned short) );
	unsigned short hist_tail = pred->hist_tail;



	int i=0;
	for(i=0; i< hist_size; i+=2 ){
		hist[i] = pred->history[hist_tail]; //phase id
		hist[i+1] = pred->history[hist_tail+1]; //phase_cntr
		hist_tail = (hist_tail+2)%(hist_size);
	}
	
	for( i=0; i < pred->future_len; i++){

	/*printf(" predictor hitory (id, num) = ");
	int index=0;
	for(index=0; index< pred->history_length*2; index+=2 ){
		printf("(%d, %d), ", hist[index] 
				    ,hist[index+1] //phase_cntr
		      );
	}
	printf("\n");*/
#ifdef USE_JENKINS_HASH
	unsigned int hash_t = hash( (unsigned char*) hist 
		// in bytes (counter+phase_id+unsigned short)
		   , hist_size*2 
		//last value can be arbitrary	   
		   , 0 ); 
#else

#endif

	//printf("hash = %d \n", hash_t);

	int phase = -1;
	//predict
	if(  ( hash_t & TAG_MASK )>> TAG_SHIFT ==
		pred->predict_table[hash_t & INDEX_MASK].tag ){ 
		// tag matches		

		phase = 
			pred->predict_table[hash_t & INDEX_MASK].phase_id;
		pred->new_phase_predictions++; // stats
	}
	else{ // in case the tag doesn't match take the last phase id
		phase = pred->history[pred->hist_head];
		//phase = -1;
		pred->last_phase_predictions++;// stats
	}

	future_seq[i] = phase; // write the prediction to output
	//printf(" predicted %dth phase = %d \n", i, phase );
	//printf(" predicted %dth futur_seq = %d \n", i, future_seq[i]);



	if( phase == hist[hist_size-2] )
		hist[hist_size-1]++; // for next hash
	else if(phase == -1) ;
	else{
		// shadow table be updated here if necessary.


	     // with a history_length of 1, it's only the last phase
	     if( (hist_size-2) > 0 ){  
		int index=0;
		for(index=0; index< (hist_size-2); index += 2){
			hist[index] = hist[index+1];
			hist[index+1] = hist[index+2];
		}
	     }

		hist[hist_size-2]= phase; // last phase
		hist[hist_size-1]= 1;
	}
		
	} // end for( pred->future_lenght

	free(hist);

	//return future_seq;
}


/*
 *
 */
void predictor_updatepredictionandhistory( struct PredictorHash *pred
				, unsigned int new_id
				){

	// copy the history for local use --- [phase-id][phase-cntr]
	// each entry contains a phase_id and phase_cntr
	unsigned int hist_size = pred->history_length * 2; 
	unsigned short *hist = (unsigned short *) 
		malloc( hist_size * sizeof(unsigned short) );
	unsigned short hist_tail = pred->hist_tail;
	int i=0;
	for(i=0; i< pred->history_length*2; i+=2 ){
		hist[i] = pred->history[hist_tail]; //phase id
		hist[i+1] = pred->history[hist_tail+1]; //phase_cntr
		hist_tail = (hist_tail+2)%(pred->history_length*2);
	}

	unsigned int hash_t = hash( (unsigned char*) hist 
			// in bytes (counter+phase_id+unsigned short)
			   , hist_size*2 
			   , 0 ); //last value can be arbitrary

	//check the tag at history's index and 
	////what if only one(tag or predict)'s different
	////if same, do nothing else update the table
	if( (hash_t & TAG_MASK)>>TAG_SHIFT == 
			pred->predict_table[ hash_t & INDEX_MASK].tag ){
		//tag match
		//compare the prediction with new_id
		if( new_id == 
			pred->predict_table[ hash_t & INDEX_MASK].phase_id ){
			// id matches after tag match. all's fine
		}
		else{
			// id doesn't match. history changed update the table
			pred->predict_table[ hash_t & INDEX_MASK].phase_id = 
				new_id;
		}

	}
	else{ // tag mismatch
		// update tag and prediction
		//predict_table[ hash_t & INDEX_MASK].tag = 
		//			(hash_t & TAG_MASK)>>TAG_SHIFT;
		//predict_table[ hash_t & INDEX_MASK].phase_id = new_id;

		//printf("updatepredictionandhistory(): tag mismatch\n");
	}

	//
	//see if the new_id's same as last phase_id
	//if yes, update the last_phase_counter
	if( new_id == pred->history[pred->hist_head] ) 
		pred->history[pred->hist_head+1]++;
	//if no, move the last phasencntr to previous
	else { 
		// phase prediction paper says insert only when id_change
		pred->predict_table[ (hash_t & INDEX_MASK)].tag = 
					(hash_t & TAG_MASK)>>TAG_SHIFT;
		pred->predict_table[ hash_t & INDEX_MASK].phase_id = new_id;

		pred->hist_head = // adjust head
			(pred->hist_head+2)%(pred->history_length*2); 

		pred->history[pred->hist_head] = new_id;
		pred->history[pred->hist_head+1] = 1;
		if(pred->hist_head == pred->hist_tail)
			pred->hist_tail =  //adjust tail
				(pred->hist_tail+2)%(pred->history_length*2);

	}


	free(hist);
}


