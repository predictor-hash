#include "predictorhash2.h"
#include "jenkins_hash.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define USE_JENKINS_HASH

struct PredictorHash * predictor_create( 
		  unsigned int hist_size
		  ,unsigned int pred_futur_len
		  ,unsigned int kept_futur_len
		  ,unsigned int kept_tag_len   // in bits
		  ,unsigned int table_size
		){

	int i=0;

	// allocate main predict struct
	struct PredictorHash *pred = (struct PredictorHash *) 
		malloc( sizeof(struct PredictorHash) );

	// allcocate prediciton table
	pred->predict_table = (struct predict_table_entry*)
		malloc( sizeof(struct predict_table_entry)*table_size );
	
	// allocate entries in prediction table
	for(i=0; i< table_size; i++){
		pred->predict_table[i].tag = 
			malloc( sizeof(unsigned int) );
		pred->predict_table[i].phase_id = 
			malloc( kept_futur_len*sizeof(unsigned int) );
	}

	pred->pred_future_len = pred_futur_len;	
	pred->kept_futur_len = kept_futur_len;	
	pred->kept_tag_len_bits = kept_tag_len;  // in bits	
	pred->kept_tag_var_len  = 1;
	pred->table_size = table_size;    // in powers of two

	pred->temp_kept_futur = (unsigned int *)
			malloc ( sizeof(unsigned int)*kept_futur_len );

	pred->pred_future_len = pred_futur_len;
	pred->history_length = hist_size;

	//initialize the hitory
	pred->history = (unsigned short *) 
		malloc( (pred->history_length+pred->kept_futur_len)
			* sizeof(unsigned short) );



	for(i=0; i< (pred->history_length+pred->kept_futur_len);
		       	i++ ){
		pred->history[i] = i; //phase id
	}

	pred->hist_head = 0; // to be advanced by 1 
	pred->hist_tail = 0;

	//initialize the hash etc
	//predict_table = (predict_table_entry *) 
	//malloc( PREDICT_TABLE_SIZE*sizeof(predict_table_entry) );
	for( i=0; i< table_size; i++){
		int j=0; 

		for(j=0; j< pred->kept_tag_var_len; j++)
			pred->predict_table[i].tag[j] = 0;

		for(j=0; j< pred->kept_futur_len; j++)
			pred->predict_table[i].phase_id[j] = j;
	}

	//stats
	pred->new_phase_predictions = 0;
	pred->last_phase_predictions = 0;

	// masks
	pred->index_mask = calc_index_mask( table_size );
	pred->tag_mask = calc_tag_mask( kept_tag_len );
	pred->tag_shift = calc_tag_shift( table_size );

	return pred;     //check for NULL Ptr above , memory allocation
}

/*
 *
 */
void predictor_printhistory( struct PredictorHash * pred){

	printf(" ********* PREDICTOR *********** \n");
	if( pred == NULL ){
		printf(" predictor_printhistory: bad ptr error \n");
	}

	//initialize the hitory
	printf(" predictor hitory (id, num) = ");
	int i=0;
	int tail = pred->hist_tail;
	for(i=0; i< pred->history_length*2; i+=2 ){
		printf("(%d, %d), ", pred->history[tail] 
				    ,pred->history[tail+1] //phase_cntr
		      );
		tail = (tail+2)%(pred->history_length*2);
	}
	printf("\n");
	
	// to be advanced by 2
	printf(" history head = %d \n",pred->hist_head); 
	printf(" history tail = %d \n",pred->hist_tail); 

	printf(" ********* PREDICTOR  END *********** \n");

}

/*
 *
 */
void predictor_print( struct PredictorHash * pred){

	printf(" ********* PREDICTOR *********** \n");
	if( pred != NULL ){
		printf(" predictor: history_length = %d \n", 
				pred->history_length);
		printf(" predictor: future length  = %d \n", 
				pred->pred_future_len);

	//initialize the hitory
	printf(" predictor hitory = ");
	int i=0;
	for(i=0; i< pred->history_length; i++ ){
		printf("(%d), ", pred->history[i] 
		      );
	}
	printf("\n");
	// to be advanced by 2
	printf(" history head = %d \n",pred->hist_head); 
	printf(" history tail = %d \n",pred->hist_tail); 

	//initialize the hash etc
	//predict_table = (predict_table_entry *) 
	//malloc( PREDICT_TABLE_SIZE*sizeof(predict_table_entry) );
	printf(" predictor table (tag , id) = ");
	for( i=0; i< pred->table_size; i++){
		int ndx=0;
		printf("%d: ",i);

		for( ndx=0; ndx < pred->kept_tag_var_len; ndx++)
			printf("(%d), ", pred->predict_table[i].tag[0]
		      );
		
		printf(" -> ");

		for( ndx=0; ndx < pred->kept_futur_len; ndx++)
			printf("%d, ", pred->predict_table[i].phase_id[ndx]);
		printf("\n");
	}
	printf("\n");

	// to be advanced by 2
	printf(" num new predictions = %d \n",pred->new_phase_predictions); 
	printf(" num last phase predictions = %d \n",
			pred->last_phase_predictions); 
	}
	else
		printf(" predictor is NULL \n");
	printf(" ********* PREDICTOR  END *********** \n");

}


/*
 *
 */
void predictor_destroy( struct PredictorHash * pred){

	if( pred ){
		if( pred->history )
			free( pred->history );

		int i=0;
		for(i=0; i< pred->table_size; i++){
			free( pred->predict_table[i].tag );
			free( pred->predict_table[i].phase_id );
		}

		free( pred->predict_table );
		free( pred );
	}
}


/*
 *
 */
void predictor_predictfutureintervals( struct PredictorHash * pred
				,unsigned int * future_seq
				){
#ifdef DEBUG_PREDICTOR_PREDICTION
	printf("********* Making prediction: *********\n");
#endif
	unsigned int greater_hist_len = 
		pred->history_length+pred->kept_futur_len;

	unsigned int hist_size = pred->history_length; 
	unsigned short *hist = (unsigned short *) 
		malloc( pred->history_length * sizeof(unsigned short) );
	unsigned short hist_tail = (pred->hist_tail+pred->kept_futur_len)%
	       			(greater_hist_len);
#ifdef DEBUG_PREDICTOR_PREDICTION
	printf("hist_size = %d\n", hist_size);
	printf("greater_hist_size = %d\n", greater_hist_len);
	printf("hist_tail = %d\n", hist_tail);
#endif



	int i=0;
	for(i=0; i< hist_size; i++ ){
		hist[i] = pred->history[hist_tail]; //phase id
		hist_tail = (hist_tail+1)%(greater_hist_len);
	}
	
#ifdef DEBUG_PREDICTOR_PREDICTION
	printf("using history -> ");
	for(i=0; i< hist_size; i++)
		printf("%d, ", hist[i]);
	printf("\n");

	printf("tag_mask = %x \n", pred->tag_mask);
	printf("tag_shift= %x \n", pred->tag_shift);
	printf("index_mask= %x \n", pred->index_mask);
#endif


	i=0;
	while(i < pred->pred_future_len){

	// hash the history
#ifdef USE_JENKINS_HASH
	unsigned int hash_t = hash( (unsigned char*) hist 
		// in bytes (counter+phase_id+unsigned short)
		   , hist_size*2 
		//last value can be arbitrary	   
		   , 0 ); 
#else
#endif

#ifdef DEBUG_PREDICTOR_PREDICTION
	printf("hash = %x \n", hash_t);
	printf("tag = %x\n",  (hash_t >> pred->tag_shift) & pred->tag_mask );
	printf("index = %d\n", (hash_t & pred->index_mask) );
#endif
	//predict
	if(  ( (hash_t >> pred->tag_shift) & pred->tag_mask ) ==
		pred->predict_table[hash_t & pred->index_mask].tag ){ 
		// tag matches		
#ifdef DEBUG_PREDICTOR_PREDICTION
		printf("tag match!\n");
		printf("prediction:");
#endif
		int ndx = 0;
		for(ndx=0; ndx < pred->kept_futur_len; ndx++){
			future_seq[i++] = 
			pred->predict_table[hash_t & pred->index_mask].phase_id[ndx];
#ifdef DEBUG_PREDICTOR_PREDICTION
			printf("%d, "
		,pred->predict_table[hash_t & pred->index_mask].phase_id[ndx] );	
#endif
			pred->new_phase_predictions++; // stats
		}
	}
	else{ // in case the tag doesn't match take the last phase id

		// or simply repeating the last phase would suffice ??
		int last_ndx = pred->history_length- (pred->kept_futur_len) ;
#ifdef DEBUG_PREDICTOR_PREDICTION
		printf("tag match NOT!\n");
		printf("last_ndx =%d\n", last_ndx);
		printf("prediction:");
#endif
		int ndx = 0;
		for(ndx=0; ndx< pred->kept_futur_len; ndx++){
			future_seq[i++] = 
				hist[(last_ndx+ndx)];
#ifdef DEBUG_PREDICTOR_PREDICTION
			printf("%d, "
			, hist[(last_ndx+ndx)] );	
#endif
			pred->last_phase_predictions++;// stats
		}
	}
#ifdef DEBUG_PREDICTOR_PREDICTION
	printf("\n");
#endif


	// update the temp history constructed

	     // with a history_length of 1, it's only the last phase
	int index = 0;
	for(index=0; index< (hist_size-pred->kept_futur_len); 
			index += pred->kept_futur_len ){
		int sub_ndx = 0;
		for( ; sub_ndx < pred->kept_futur_len; sub_ndx++)
			hist[index+sub_ndx] = 
				hist[index+sub_ndx+pred->kept_futur_len];
	}

	int sub_ndx = 0;
	index = index - (pred->kept_futur_len-1);
#ifdef DEBUG_PREDICTOR_PREDICTION
	printf("index = %d \n", index );
	printf("Adding end of history = ");
#endif
	for( ; sub_ndx < pred->kept_futur_len; sub_ndx++){
		hist[index+sub_ndx] = future_seq[i-pred->kept_futur_len+sub_ndx];
#ifdef DEBUG_PREDICTOR_PREDICTION
		printf("%d, ", future_seq[i-pred->kept_futur_len+sub_ndx] );
#endif
	}
#ifdef DEBUG_PREDICTOR_PREDICTION
	printf("\n");
	printf(" new temp history -> ");
	for(index=0; index< (hist_size); index++)
	       printf("%d, ", hist[index]);	
	printf("\n");
#endif

	} // end while

	free(hist);

#ifdef DEBUG_PREDICTOR_PREDICTION
	printf("********* Making prediction End *********\n");
#endif

	//return future_seq;
}


/*
 *
 */
void predictor_updatepredictionandhistory( struct PredictorHash *pred
				, unsigned int new_id
				){

	// copy the history for local use --- [phase-id][phase-cntr]
	// each entry contains a phase_id and phase_cntr
#ifdef DEBUG_PREDICTOR_UPDATE
	printf("************ Updating predictor ***************\n");
#endif
	unsigned int hist_size = pred->history_length; 
	unsigned int greater_hist_len = pred->history_length+pred->kept_futur_len; 
	unsigned short *hist = (unsigned short *) 
		malloc( hist_size * sizeof(unsigned short) );
	unsigned short hist_tail = pred->hist_tail;
	int i=0;
	for(i=0; i< pred->history_length; i++ ){
		hist[i] = pred->history[hist_tail]; //phase id
		hist_tail = (hist_tail+1)%(pred->history_length);
	}

#ifdef DEBUG_PREDICTOR_UPDATE
	printf("using history = ");
	for(i=0; i< pred->history_length; i++ )
		printf("%d, ", hist[i]);
	printf("\n");
#endif

	unsigned int hash_t = hash( (unsigned char*) hist 
			// in bytes (counter+phase_id+unsigned short)
			   , hist_size 
			   , 0 ); //last value can be arbitrary
#ifdef DEBUG_PREDICTOR_UPDATE
	printf("hast = %x\n", hash);
#endif


	int last_ndx = (pred->hist_head - (pred->kept_futur_len-1) ) %
				pred->history_length;
#ifdef DEBUG_PREDICTOR_UPDATE
	printf("last_ndx = %d\n", last_ndx);
#endif
	for(i=0; i< pred->pred_future_len-1; i++)
		pred->temp_kept_futur[i] = 
			pred->history[(last_ndx+i)%greater_hist_len];

	pred->temp_kept_futur[pred->pred_future_len-1] = new_id;
#ifdef DEBUG_PREDICTOR_UPDATE
	printf("temp_kept_futur = ");
	for(i=0; i< pred->pred_future_len; i++)
		printf("%d, ", pred->temp_kept_futur[i]);
	printf("\n");
#endif
	
	//check the tag at history's index and 
	////what if only one(tag or predict)'s different
	////if same, do nothing else update the table
	if( ((hash_t >> pred->tag_shift) & pred->tag_mask) == 
			pred->predict_table[ hash_t & pred->index_mask].tag ){
		//tag match

		//compare the prediction with new_id
		// get the history's last two id's

		if( compare_two_arrays( pred->temp_kept_futur
			, pred->predict_table[ hash_t & pred->index_mask].phase_id
			, pred->kept_futur_len
			, 60.0 // success measure
			) 
		) {
			// id matches after tag match. all's fine
		}
		else{
			// id doesn't match. history changed update the table
			for(i=0; i< pred->kept_futur_len; i++)
			pred->predict_table[ hash_t & pred->index_mask].phase_id[i] = 
				pred->temp_kept_futur[i];
		}

	}
	else{ // tag mismatch
		// update tag and prediction
		//predict_table[ hash_t & pred->index_mask].tag = 
		//			(hash_t & pred->tag_mask)>>pred->tag_shift;
		//predict_table[ hash_t & pred->index_mask].phase_id = new_id;

		//printf("updatepredictionandhistory(): tag mismatch\n");
	}

	// phase prediction paper says insert only when id_change
	pred->predict_table[ (hash_t & pred->index_mask)].tag[0] = 
				((hash_t >> pred->tag_shift ) & pred->tag_mask);
	for(i=0; i< pred->kept_futur_len; i++)
	pred->predict_table[ hash_t & pred->index_mask].phase_id[i] = 
		pred->temp_kept_futur[i];


	// adjust head and tail
	pred->hist_head = // adjust head
		(pred->hist_head+1)%(greater_hist_len); 
	pred->history[pred->hist_head] = new_id;

	if(pred->hist_head == pred->hist_tail)
		pred->hist_tail =  //adjust tail
			(pred->hist_tail+1)%(greater_hist_len);


#ifdef DEBUG_PREDICTOR_UPDATE
	printf("************ Updating predictor END ***************\n");
#endif

	free(hist);
}


unsigned int compare_two_arrays( unsigned int *a1, unsigned int *a2
				 ,int len, float success_measure ){
	int i=0;
	int equals = 0;
	for(i=0; i< len; i++)
		if(a1[i] == a2[i])
			equals++;

	float match = (float)equals/(float)len;

	return match > success_measure;
}
/******************************************************************************
Function:    _round_up_pow2
Description: rounds an int up to the next power of two
Inputs:      int a - the number
Outputs:     int - the power of 2 greater or equal to a
Start Date:  22 MAY 06 jkihm
Mods:        
******************************************************************************/
int _round_up_pow2(int a){
  int ans = 1, num_ones = 0;

  while(a > 1){
    if(a & 1) num_ones++;
    a >>= 1;
    ans <<= 1;
  }

  if(num_ones > 0){
    ans <<=1;
  }

  return ans;
}//_round_up_pow2

unsigned int calc_index_mask( unsigned int table_size){
	return table_size-1;
}

unsigned int calc_tag_mask( unsigned int kept_tag_len ){
	double power = pow( 2.0, (float)kept_tag_len ); 
	return power-1;
}

unsigned int calc_tag_shift( unsigned int table_size){
	unsigned int shift = 0;
	while( table_size ){
		shift++;
		table_size >>= 1;
	}
	return shift-1;
}
