all:
	gcc -o predictor_test main.c  options.c  predictorhash2.c miscc.c -lm
run:
	./predictor_test  -histlen 20 \
		-futurlen 6 \
		-pdtr_keptfuturlen 6 \
		-pdtr_tagbits 6 \
		-pdtr_tablesize 256 \
		-trace ../traces/gzip.ref.source.tdp

clean:
	rm predictor_test *~
