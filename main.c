#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "miscc.h" // TRUE FALSE
#include "predictorhash2.h"
#include "options.h"

static int arg_histlen;
static int arg_futurlen;
static char* arg_tracefile;
static char* arg_dumpfile;
static int arg_predkeptfuturlen;
static int arg_predtagbits;
static int arg_predtablesize;

// ddummy comment
void 
pred_reg_options( struct opt_odb_t *odb){

	opt_reg_string(odb, "-trace"
			, "input file with phase-trace"
			, &arg_tracefile
			, "trace.tdp"
			, /* print */TRUE
			, NULL);

	opt_reg_int(odb , "-histlen"
			, "history len"
			, &arg_histlen
			, /* default */1
			, /* print */TRUE
			, /* format */NULL);

	opt_reg_int(odb , "-futurlen"
			, "futur len"
			, &arg_futurlen
			, /* default */1
			, /* print */TRUE
			, /* format */NULL);
	
	opt_reg_string(odb, "-dump"
			, "output file with predicted-phase-trace"
			, &arg_dumpfile
			, "dump.tdp"
			, /* print */TRUE
			, NULL);

	opt_reg_int(odb , "-pdtr_keptfuturlen"
			, "futur len kept in predictor"
			, &arg_predkeptfuturlen
			, /* default */3
			, /* print */TRUE
			, /* format */NULL);
	
	opt_reg_int(odb , "-pdtr_tagbits"
			, "tag bits kept in predictor"
			, &arg_predtagbits
			, /* default */6
			, /* print */TRUE
			, /* format */NULL);
	
	opt_reg_int(odb , "-pdtr_tablesize"
			, "tag bits kept in predictor"
			, &arg_predtablesize
			, /* default */256
			, /* print */TRUE
			, /* format */NULL);
	
}

unsigned int
get_next_phase( FILE* tracefile ){

	char string[100];
	fgets(string, 100, tracefile);
	if( feof(tracefile) ) return -1; // file nished

	char* ptoken = NULL;
	ptoken = strtok( string, " "); // Sim/Emu
	//printf("1st_token = %s\n", ptoken);
	ptoken = strtok( NULL, " ");   // interval_id
	//printf("2nd_token = %s\n", ptoken);
	ptoken = strtok( NULL, " ");   // label
	//printf("3rd_token = %s\n\n\n", ptoken);

	unsigned int label = atoi( ptoken );

	return label;
}

static int exec_index = -1;

static int
orphan_fn(int i, int argc, char **argv)
{
  exec_index = i;
  return /* done */FALSE;
}

int main( int argc, char* argv[]){

	unsigned int head = 0;
	unsigned int tail = 0;
	unsigned long long num_predictions = 0;
	
	struct opt_odb_t *my_odb = NULL;
	my_odb = opt_new(orphan_fn);
	pred_reg_options(my_odb);
	opt_process_options(my_odb, argc, argv);

	printf(" predictor_futurlen = %d\n", arg_futurlen);
	printf(" predictor_histlen = %d\n", arg_histlen);
	printf(" predictor trace   = %s\n", arg_tracefile);
	// create predictor
	struct PredictorHash * predictor = 
		predictor_create( arg_histlen  // hist size
				, arg_futurlen // futur len
				, 3		// kept futurlen
				, 6		// kept tag len in bits
				, 256		// entries in table
				);

#ifdef DEBUG_PREDICTOR
	predictor_print( predictor );
#endif

	// make futur and history arrays
	unsigned int *trace_intervals = (unsigned int*)
		malloc( arg_futurlen*sizeof(unsigned int) );

	unsigned int *predicted_intervals = (unsigned int*)
		malloc( arg_futurlen*sizeof(unsigned int) );

	unsigned int *predict_stats = (unsigned int*)
		malloc( arg_futurlen*sizeof(unsigned int) );
	//INITIZLIZE
	int i=0;// get the 1st ten values.
	for(i=0; i< arg_futurlen; i++) 
		trace_intervals[i] = predicted_intervals[i] =
			predict_stats[i] = 0;

	// open trace file
	FILE* ptrace = fopen( arg_tracefile, "r");
	FILE* pdump = fopen( arg_dumpfile, "w");
	fprintf(pdump, "#Emu interval label\n");
	

	char string[100];
	fgets( string, 100, ptrace); // get header

	for(i=0; i< arg_futurlen; i++){ 
		trace_intervals[head] = get_next_phase (ptrace);
		//predictor_updatepredictionandhistory(predictor
	        //		, trace_intervals[head]);
		//fprintf(pdump, "Emu %llu %d\n"
	        //		, num_predictions
		//		, trace_intervals[head]
		//      );
			       	

		//num_predictions++;
		if( head == tail) tail = ++tail%arg_futurlen;
		head = ++head%arg_futurlen;
	}
	printf("after init: head= %d, tail =%d\n", head, tail);
	// loop: make predictions and compare
	while( !feof(ptrace) ){

		predictor_predictfutureintervals( predictor
				, predicted_intervals );

#ifdef DEBUG_PREDICTOR
		printf("predicted_intervals -> ");
		for(i=0; i< arg_futurlen; i++)
			printf("%d, ", predicted_intervals[i]);
		printf("\n");
#endif
		// compare trace to prediction
		unsigned int temp_tail = tail;
		for( i=0; i< arg_futurlen; i++){

		   printf("trace[%d]=%d, pred[%d]=%d.\n"
				   ,(head+i)%arg_futurlen
				   , trace_intervals[(head+i)%arg_futurlen]
				   , i
				   ,predicted_intervals[i]
			 );
		   if( trace_intervals[(head+i)%arg_futurlen] == 
				   predicted_intervals[i] ){
			predict_stats[i]++;
			temp_tail = ++temp_tail%arg_futurlen;
		   }
		}
		printf("\n\n\n");

		num_predictions++;
		fprintf(pdump, "Emu %llu %d\n"
				, num_predictions
				, predicted_intervals[0]
		       );
			       	
		unsigned int label = get_next_phase(ptrace);
		trace_intervals[head%arg_futurlen] = label;
		// eof checked next in while condition

		predictor_updatepredictionandhistory(predictor
				, trace_intervals[(head+1)%arg_futurlen]
				);

#ifdef DEBUG_PREDICTOR
		predictor_print( predictor );
#endif

		if( head == tail) tail = ++tail%arg_futurlen;
		head = ++head%arg_futurlen;
	}


	// print output stats
	printf("total_predictions = %llu\n", num_predictions);

	printf("predictions seq-historgram%: ");
	for(i=0; i< arg_futurlen; i++)
		printf("%f, ", (float)predict_stats[i]/(float)num_predictions);
	printf("\n");

	fclose(ptrace);
	fclose(pdump);
	return 0;
}
