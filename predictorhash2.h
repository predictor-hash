/*****************************************************/
/***** Same as predictorhash.h, but table is param ***/
/***** etrizable here                              ***/
/*****************************************************/


#ifndef PREDICTORHASH_H
#define PREDICTORHASH_H



#define PREDICT_TABLE_SIZE 0x1000 // 256 entry table
#define INDEX_MASK 0x00000FFF // 8 bit index in a 256 entry table
#define TAG_MASK   0x00003000 // next 6 bits for tag
#define TAG_SHIFT  12	      // shift to bring the tag down for comparison.
#define HASH_SHIFT 5 // shift the prev_hash by this many bits before ORing with last_hash

struct predict_table_entry{
/*Phase prediction paper says that their predictor has 
256 entries encompassing less than 500 bytes. This should make each entry of
2 bytes (a little less).  How to divide this between tag and predicted id ?

On-line simpoint says that they have a signature table of 1024 entries. Thus
the phase_id can go up to 10 bits. This leaves only 6 bits for Tag ?  Or did
they use larger table entries ?
*/

	// limit the size in the light of above discussion?
	unsigned int *tag;	
	unsigned int *phase_id;
};

struct PredictorHash {

// how much in future we have to predict
	unsigned int pred_future_len;	
	unsigned int kept_futur_len;	
	unsigned int kept_tag_len_bits; // in bits	
	unsigned int kept_tag_var_len;  // var len. 1 ftm
	unsigned int table_size;    // in powers of two

	// based on table size
	unsigned int index_mask;
	unsigned int tag_mask;
	unsigned int tag_shift;


	unsigned short * history;
	unsigned int hist_head;
	unsigned int hist_tail;

	unsigned int *temp_kept_futur;	
	/*it shows only the num-entries. size of history = hist_len*2*2 bytes
	 * if short */
	unsigned int history_length; 
	int largest_center;


	// prediction table
	// see the size : shall we limit the size ??
	struct predict_table_entry *predict_table; 
	unsigned int prev_hash;       // contains the id and num_occr of 
				      // prev (before the last) phase hashed

	unsigned int last_phase_predictions; //stats
	unsigned int new_phase_predictions;
	
};

struct PredictorHash * predictor_create( 
		  unsigned int hist_size
		  ,unsigned int pred_futur_len
		  ,unsigned int kept_futur_len
		  ,unsigned int kept_tag_len
		  ,unsigned int table_size
		);

unsigned int calc_index_mask( unsigned int table_size);
unsigned int calc_tag_mask( unsigned int table_size);
unsigned int calc_tag_shift( unsigned int table_size);

unsigned int compare_two_arrays( unsigned int *a1, unsigned int *a2
				 , int len, float success_measure );
#endif //PREDICTORHASH_H
